# Copyright(c) 2017 by craftyguy "Clayton Craft" <clayton@craftyguy.net>
# Distributed under GPLv3+ (see COPYING) WITHOUT ANY WARRANTY.

import asyn
from micropython import kbd_intr
import uasyncio as asyncio
import utime
from machine import RTC, DEEPSLEEP, deepsleep, Pin, unique_id, UART
from umqtt.simple import MQTTClient


###### Configuration settings ######
PROJECT = "esp-botvac"
MQTT_SERVER = "1.1.1.1"
DO_DEEP_SLEEP = True
# Time (in seconds) to stay in deep sleep
SLEEP_TIME = const(180)
# Time (in seconds) to stay awake (before deep sleep)
# Note: this should be > 30 seconds
AWAKE_TIME = const(30)
# MQTT TOPICS
TOPIC_PFX = "home/neato/botvac"
TOPIC_PLAYSOUND = "/playsound"
TOPIC_SETSCHEDULE = "/setschedule"
TOPIC_SCHEDULE = "/schedule"
TOPIC_CLEANING_STATE = "/cleaning_state"
TOPIC_CHARGING_STATE = "/charging_state"
TOPIC_CMD = "/cmd"
TOPIC_VACUUM = "/clean"
TOPIC_DEBUG = "/debug"
TOPIC_DEBUG_ENABLE = "/debug/enable"
# Polling intervals (in seconds)
POLL_SCHEDULE = const(600)
POLL_STATE = const(60)
POLL_SUBS = const(30)
# Start delay, in seconds, to wait for vacuum to boot
START_DELAY = const(10)

###### Global vars ######
umqtt_uid = PROJECT + "_" + ''.join('%02X' % b for b in unique_id())
umqtt_client = MQTTClient(umqtt_uid, MQTT_SERVER)
mqtt_connected = False
# Locks for accessing mqtt client and serial device
umqtt_lock = asyn.Lock()
serial_lock = asyn.Lock()
# If this pin is low, then debug messages will print to serial console,
# else they will print to mqtt debug topi
debug_p = Pin(2, Pin.IN)
# uart speed doesn't seem to matter for the botvac
uart = UART(0, 115200)
# Default enable or disable debug messages over MQTT
mqtt_debug_out = False
# Coroutine loop
loop = asyncio.get_event_loop()


# do_serial: Send command (cmd) over uart and return results.
# If using coroutines, a lock should be used to insure that
# this function is not called multiple times concurrently.
# Note: return value is a byte array
def serial_do(cmd):
    uart.init(115200, bits=8, parity=None, stop=1)
    read_str = b''
    # print() on esp8266 writes to UART0
    print(cmd)
    # give botvac time to respond
    utime.sleep_ms(200)
    try:
        # Disable ctrl+c
        kbd_intr(-1)
        read_str = uart.read()
    except Exception as e:
        # TODO: handle this better..
        pass
    finally:
        # re-enable ctrl+c
        kbd_intr(3)
    if read_str is None:
        read_str = b''
    return read_str


# mqtt_check_subs: monitor for new messages on mqtt subscription
async def mqtt_check_subs(mlock):
    global umqtt_client
    global mqtt_connected
    do_loop = True

    while do_loop:
        if DO_DEEP_SLEEP:
            # In deep sleep mode, this only runs once
            do_loop = False
            await asyncio.sleep(5)
        else:
            await asyncio.sleep(POLL_SUBS)
        async with mlock:
            if mqtt_connected:
                try:
                    umqtt_client.check_msg()
                    umqtt_client.publish(TOPIC_PFX + TOPIC_CMD, '',
                                         retain=True)
                    umqtt_client.publish(TOPIC_PFX + TOPIC_DEBUG_ENABLE, '',
                                         retain=True)
                    umqtt_client.publish(TOPIC_PFX + TOPIC_PLAYSOUND, '',
                                         retain=True)
                    umqtt_client.publish(TOPIC_PFX + TOPIC_SETSCHEDULE, '',
                                         retain=True)
                    umqtt_client.publish(TOPIC_PFX + TOPIC_VACUUM, '',
                                         retain=True)
                except Exception as e:
                    await print_d("Exception: {0}".format(e))
                    loop.create_task(mqtt_connect(mlock))


#  mqtt_sub_cb: callback for mqtt client subscriptions. Unknown topics
# are ignored
def mqtt_sub_cb(topic, msg):
    global loop
    global serial_lock
    global mqtt_debug_out

    t = topic.decode('ASCII').lower()
    m = msg.decode('ASCII').lower()
    if t == TOPIC_PFX + TOPIC_SETSCHEDULE:
        loop.create_task(botvac_set_schedule(m, serial_lock))
    elif t == TOPIC_PFX + TOPIC_PLAYSOUND:
        loop.create_task(botvac_play_sound(m, serial_lock))
    elif t == TOPIC_PFX + TOPIC_VACUUM:
        loop.create_task(botvac_vacuum(m, serial_lock))
    elif t == TOPIC_PFX + TOPIC_DEBUG_ENABLE:
        if m == "on":
            mqtt_debug_out = True
        elif m == "off":
            mqtt_debug_out = False
    elif t == TOPIC_PFX + TOPIC_CMD:
        loop.create_task(botvac_cmd(m, serial_lock))
    return


# mqtt_connect: Establish connection to mqtt broker. If connection fails,
# it will try to connect every 5 seconds
async def mqtt_connect(mlock):
    global umqtt_client
    global mqtt_connected
    retry_delay = 5
    umqtt_client.DEBUG = False
    umqtt_client.set_callback(mqtt_sub_cb)
    mqtt_connected = False
    async with mlock:
        while not mqtt_connected:
            try:
                umqtt_client.connect(clean_session=True)
                umqtt_client.subscribe(TOPIC_PFX + TOPIC_CMD)
                umqtt_client.subscribe(TOPIC_PFX + TOPIC_DEBUG_ENABLE)
                umqtt_client.subscribe(TOPIC_PFX + TOPIC_PLAYSOUND)
                umqtt_client.subscribe(TOPIC_PFX + TOPIC_SETSCHEDULE)
                umqtt_client.subscribe(TOPIC_PFX + TOPIC_VACUUM)
                mqtt_connected = True
                await print_d('MQTT connected!')
            except Exception as e:
                await print_d("Exception: {0}".format(e))
                # Unable to connect, retry
                await asyncio.sleep(retry_delay)


async def botvac_cmd(cmd, slock):
    async with slock:
        # send arbitrary command to botvac, return data is ignored
        serial_do(cmd)


async def botvac_set_schedule(sched, slock):
    async with slock:
        # send command, return is ignored
        serial_do("SetSchedule {0}".format(sched))
    return


async def botvac_set_time(datetime, slock):
    async with slock:
        # send command, return is ignored
        serial_do("SetTime {0}".format(datetime))
    return


async def botvac_play_sound(sid, slock):
    try:
        int(sid)
    except Exception as e:
        # Invalid (NaN, empty) sound IDs are simply ignored
        return
    async with slock:
        # send command, return is ignored
        serial_do("PlaySound {0}".format(sid))
    return


async def botvac_vacuum(cmd, slock):
    async with slock:
        if cmd == 'on':
            serial_do("Clean {0}".format('House'))
        elif cmd == 'off':
            serial_do("Clean {0}".format('Stop'))
    # unrecognized commands are simply ignored
    return


async def botvac_get_schedule(mlock, slock):
    global umqtt_client
    global mqtt_connected
    do_loop = True
    while do_loop:
        if DO_DEEP_SLEEP:
            # In deep sleep mode, this only runs once
            await asyncio.sleep(17)
            do_loop = False
        else:
            await asyncio.sleep(POLL_SCHEDULE)

        async with slock:
            sched = serial_do("GetSchedule")
        sched = sched.decode('ascii')

        await print_d("current schedule: {0}".format(sched), mlock)
        async with mlock:
            if mqtt_connected:
                try:
                    umqtt_client.publish(TOPIC_PFX + TOPIC_SCHEDULE, sched)
                except Exception as e:
                    await print_d("Exception: {0}".format(e))
                    loop.create_task(mqtt_connect(mlock))


async def botvac_get_state(mlock, slock):
    global umqtt_client
    global mqtt_connected
    do_loop = True
    await print_d("Starting to monitor vacuum state", mlock)

    while do_loop:
        clean_state = ''
        charge_state = ''
        chrg_val = 0
        vac_val = 0
        s_name = ''
        s_val = ''

        if DO_DEEP_SLEEP:
            # In deep sleep mode, this only runs once
            await asyncio.sleep(13)
            do_loop = False
        else:
            await asyncio.sleep(POLL_STATE)

        async with slock:
            resp = serial_do("GetAnalogSensors")
        resp = resp.decode('ascii')
        await print_d("Analog sensors: {0}".format(resp), mlock)
        if resp == '':
            await print_d("Unable to get state, botvac still booting?", mlock)
            continue

        for sensor in resp.split('\r\n'):
            # Only interested in fields 1 & 3 in the line
            try:
                s_name, s_val = sensor.split(',')[0::2]
            except:
                # For some reason there's no valid data, possibly because the
                # botvac isn't ready yet or we received garbage
                continue
            # ExternalVoltage is a good indicator of whether the botvac is charging
            if s_name == "ExternalVoltage":
                try:
                    chrg_val = int(s_val)
                except Exception as e:
                    await print_d("Exception: {0}".format(e), mlock)
            # VacuumCurrent is a good indicator of whether the botvac is vacuuming
            elif s_name == "VacuumCurrent":
                try:
                    vac_val = int(s_val)
                except Exception as e:
                    await print_d("Exception: {0}".format(e), mlock)

        await print_d("chrg_val: {0}, vac_val: {1}".format(chrg_val, vac_val), mlock)
        # chrg_val is in mV, so 5000 == 5V, botvac charger is ~18-19V
        if chrg_val > 5000:
            await print_d("botvac is charging", mlock)
            clean_state = "OFF"
            charge_state = "ON"
        # There should be no current if vacuum motor is not running
        elif vac_val > 0:
            await print_d("botvac is vacuuming", mlock)
            clean_state = "ON"
            charge_state = "OFF"
        # catch-all condition, vacuum could also be stuck or sitting on my dev bench
        else:
            await print_d("botvac is stalled", mlock)
            clean_state = "OFF"
            charge_state = "OFF"
        # Send state to mqtt topic
        async with mlock:
            if mqtt_connected:
                try:
                    umqtt_client.publish(TOPIC_PFX + TOPIC_CLEANING_STATE, clean_state)
                    umqtt_client.publish(TOPIC_PFX + TOPIC_CHARGING_STATE, charge_state)
                except Exception as e:
                    await print_d("Exception: {0}".format(e))
                    loop.create_task(mqtt_connect(mlock))

    await print_d("botvac_get_state is ending unexpectedly!!", mlock)

# Print debug messages.
# debug_p is low: msgs printed to serial
# debug_p is high: msgs printed over mqtt TOPIC_DEBUG
# Note : If a mqtt lock isn't provided and debug_p is high, nothing
# will be printed!
async def print_d(msg, mlock=None):
    msg = "DEBUG: {0}".format(msg)
    if not debug_p.value():
        print(msg)
    elif mlock and mqtt_debug_out:
        async with mlock:
            if mqtt_connected:
                try:
                    umqtt_client.publish(TOPIC_PFX + TOPIC_DEBUG, msg)
                except Exception as e:
                    loop.create_task(mqtt_connect(mlock))


def main():
    # Set up coroutines
    loop.create_task(mqtt_connect(umqtt_lock))
    loop.create_task(mqtt_check_subs(umqtt_lock))
    loop.create_task(botvac_get_state(umqtt_lock, serial_lock))
    loop.create_task(botvac_get_schedule(umqtt_lock, serial_lock))

    # This clears out anything that may have been received in the serial buffer before now
    utime.sleep(START_DELAY)
    serial_do('')

    if DO_DEEP_SLEEP:
        rtc = RTC()
        rtc.irq(trigger=rtc.ALARM0, wake=DEEPSLEEP)
        rtc.alarm(rtc.ALARM0, SLEEP_TIME * 1000)
        loop.call_later(AWAKE_TIME, deepsleep)
    # Start main loop
    loop.run_forever()
