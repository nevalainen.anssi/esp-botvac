### Description:
# esp-botvac

Neato Botvac D80/85 controller using an ESP8266 and MQTT


### Summary:
This project uses an ESP8266, operating as a MQTT client, to allow sending remote commands to and querying status of the Neato Botvac D80/85 vacuum.


### Description:
This project turns an ESP8266 into a remote control for the Neato Botvac robot vacuum. This was developed for the D80/85, but *should* work on previous models that use [Neato's robot application commands.](https://www.neatorobotics.com/resources/programmersmanual_20140305.pdf)

What currently works:

- Sending commands (playing sounds, starting / stopping whole house cleaning, setting schedule & time)

- Determining status (charging [on/off], cleaning [on/off], schedule)

What doesn't work:

- A limited amount of the Neato API is supported, and more could be added to expose more functionality


#### How to use:

The ESP8266 is controlled remotely by sending MQTT messages to a number of topics that it is subscribed to. Meanwhile, some status info is published to a few other topics so that other devices/applications (like Home Assistant) can be made aware of the botvac's status.

|MQTT Topic                             |Publish or Subscribe?  |Expected Msg Format    |
|---------------------------------------|-----------------------|-----------------------|
|home/botvac/neato/playsound            |Subscribe              |Sound ID               |
|home/botvac/neato/clean                |Subscribe              |"on" or "off"          |
|home/botvac/neato/cleaning_state       |Publish                |"on" or "off"          |
|home/botvac/neato/charging_state       |Publish                |"on" or "off"          |
|home/botvac/neato/settime              |Subscribe              |Valid time format      |
|home/botvac/neato/setschedule          |Subscribe              |Valid sched. format    |
|home/botvac/neato/schedule             |Publish                |Current schedule       |
|home/botvac/neato/debug                |Publish                |Debug information      |
|home/botvac/neato/debug/enable         |Subscribe              |"on" or "off"          |

Some notes about the MQTT interface:

- Valid formats are specified in the [Neato programmer's guide](https://www.neatorobotics.com/resources/programmersmanual_20140305.pdf)

- "Subscribe" means the ESP8266 is subscribed to this topic and waiting for a message

- "Publish" mean the ESP8266 is periodically writing messages to this topic

While it's entirely possible to use the botvac's internal scheduling mechanism (and relevant MQTT topics) for setting up scheduling, I found it far easier (from a usability standpoint) to use Home Assistant's automation mechanism to handle the scheduling and disable the internal scheduling on the botvac. In my implementation I have added the botvac/esp8266 as a "MQTT Switch", and Home Assistant basically turns it 'on' in the trigger for the automation task.

### Software Installation:

- [Build the esp-open-sdk](https://github.com/pfalcon/esp-open-sdk).

- Download micropython-lib, micropython-async, and micropython from their respective project pages.

- [Include uasyncio, umqtt, and asyn modules as frozen modules in micropython](https://learn.adafruit.com/micropython-basics-loading-modules/frozen-modules)

- Also include [micropython-async](https://github.com/peterhinch/micropython-async) (asyn.py) as a frozen module. Note: if you run out of space, consider removing the web_repl applications, or prune something else that is not a requirement here.

- Edit `boot.py` and `main.py` in this project to include your WiFi settings and MQTT broker IP, along with any other settings you would like to modify.

- Build micropython. I suggest including the files in this project under `esp8266/scripts` in your micropython directory so you don't have to manually copy these files to the device

  - Hint: You'll need python2 env. for esptool.py to run successfully at the end of the build..


- [Flash the ESP8266 using esptool](https://docs.micropython.org/en/latest/esp8266/esp8266/tutorial/intro.html#intro), using your fresh firmware-combined.bin image of course!

Power on device and send it a command! Here's an example with [Mosquitto](http://mosquitto.org/):

    mosquitto_pub -h 1.1.1.1 -t home/neato/botvac/playsound -m '1'

Note: To anyone looking to extend this program, keep in mind that the serial RX buffer on the ESP8266 is TINY, which means that some of the longer messages that the botvac may respond with are truncated and lost for all time to the serial buffer gods. Luckily this wasn't a problem for the functionality I implemented, but would definitely be a problem if you're looking to, for example, access sensor data towards the end of the GetAnalogSensors response.

### Hardware Installation:

#### BOM:

![Schematic](/schematic.png)

- Wemos D1 Mini ESP8266 module

- 200uF capacitor (for 3.3V into the ESP8266)

- 4 long wires for VCC< GND, RXD, and TXD

- D0 must be connected to RST for deep sleep to work

[Images of pinout on the Botvac can be viewed here,](https://github.com/sstadlberger/botvac-wifi/raw/master/img/botvac-pins.jpg) courtesy of sstadlberger.

The 3.3V from the botvac is sufficient to run the ESP8266, the 200uF capacitor insures that any current draw spikes from the ESP8266 can be satisfied before the botvac's power supply can respond. This prevented 'random' resets I encountered when testing.

As shown in the album below, the ESP8266 module was mounted on the top lid of the botvac, opposite to the LCD display.

[Images of installation are posted here.](http://imgur.com/a/VPFsk)
